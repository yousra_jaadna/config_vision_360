package com.sqli.app.conf.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="commun")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class ConfEntity implements Serializable {

	/**
	 * 
	 */ 
	private static final long serialVersionUID = -1622082984869395003L;
	
	@Id
	@GeneratedValue
	private long id;
	
	@Column(nullable=false)
	private String versionId;
	
	@Column(nullable=true)
	private int delai;
	
	@Column(name = "Taux.tva", nullable=true)
	private int tva;
	
	@Column(name = "Mode.paiement", nullable=true)
	private String paiement;
	
	@Column(name = "Activation.changement.mdp", nullable=true)
	private Boolean Activ;


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public int getDelai() {
		return delai;
	}

	public void setDelai(int delai) {
		this.delai = delai;
	}

	public int getTva() {
		return tva;
	}

	public void setTva(int tva) {
		this.tva = tva;
	}

	public String getPaiement() {
		return paiement;
	}

	public void setPaiement(String paiement) {
		this.paiement = paiement;
	}

	public boolean isActiv() {
		return Activ;
	}

	public void setActiv(boolean activ) {
		Activ = activ;
	}

}
