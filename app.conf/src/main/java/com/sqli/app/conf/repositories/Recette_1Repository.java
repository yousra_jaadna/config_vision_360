package com.sqli.app.conf.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sqli.app.conf.entities.Recette_1;

public interface Recette_1Repository extends JpaRepository<Recette_1, Long>{

}
