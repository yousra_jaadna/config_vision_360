package com.sqli.app.conf.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sqli.app.conf.entities.PreProduction;

public interface PreProductionRepository extends JpaRepository<PreProduction, Long>{

}
