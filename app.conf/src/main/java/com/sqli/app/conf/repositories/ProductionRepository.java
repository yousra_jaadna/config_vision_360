package com.sqli.app.conf.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sqli.app.conf.entities.Production;



public interface ProductionRepository extends JpaRepository<Production, Long>{

}
