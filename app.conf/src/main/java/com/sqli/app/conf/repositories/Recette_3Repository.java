package com.sqli.app.conf.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sqli.app.conf.entities.Recette_3;

public interface Recette_3Repository extends JpaRepository<Recette_3, Long>{

}
