package com.sqli.app.conf.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sqli.app.conf.entities.ConfEntity;

@Repository
public interface ConfRepository extends JpaRepository<ConfEntity, Long> {

}
