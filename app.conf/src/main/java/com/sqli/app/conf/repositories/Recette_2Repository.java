package com.sqli.app.conf.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sqli.app.conf.entities.Recette_2;

public interface Recette_2Repository extends JpaRepository<Recette_2, Long>{

}
