package com.sqli.app.conf.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sqli.app.conf.entities.ConfEntity;
import com.sqli.app.conf.entities.PreProduction;
import com.sqli.app.conf.entities.Production;
import com.sqli.app.conf.entities.Recette_1;
import com.sqli.app.conf.entities.Recette_2;
import com.sqli.app.conf.entities.Recette_3;
import com.sqli.app.conf.entities.TestEnv;
import com.sqli.app.conf.repositories.ConfRepository;
import com.sqli.app.conf.repositories.PreProductionRepository;
import com.sqli.app.conf.repositories.ProductionRepository;
import com.sqli.app.conf.repositories.Recette_1Repository;
import com.sqli.app.conf.repositories.Recette_2Repository;
import com.sqli.app.conf.repositories.Recette_3Repository;
import com.sqli.app.conf.repositories.TestEnvRepository;

@RestController
@RequestMapping("/conf") //localhost:8055/conf
public class ConfController {
	
	@Autowired
	private ConfRepository confRepository;
	
	@Autowired
	private TestEnvRepository testEnvRepository;
	
	@Autowired
	private ProductionRepository productionRepository;
	
	@Autowired
	private PreProductionRepository preproductionRepository;
	
	@Autowired
	private Recette_1Repository recette_1Repository;
	
	@Autowired
	private Recette_2Repository recette_2Repository;
	
	@Autowired
	private Recette_3Repository recette_3Repository;


	//get all properties of commun
	@GetMapping("/commun")
	public List<ConfEntity> getAllPropertiesCommun(){
		return confRepository.findAll();
	}
	
	//get all properties of test environnement
		@GetMapping("/TestEnv")
		public List<TestEnv> getAllPropertiesTestEnv(){
			return testEnvRepository.findAll();
		}
	//get all properties of Production environnement
		@GetMapping("/Production")
		public List<Production> getAllPropertiesProduction(){
			return productionRepository.findAll();
		}
		
	//get all properties of PreProduction
		@GetMapping("/PreProduction")
		public List<PreProduction> getAllPropertiesPreProduction(){
			return preproductionRepository.findAll();
		}
		
	//get all properties of Recette 1 
		@GetMapping("/Recette_1")
		public List<Recette_1> getAllPropertiesRecette_1(){
			return recette_1Repository.findAll();
		}
	//get all properties of Recette 2 
		@GetMapping("/Recette_2")
		public List<Recette_2> getAllPropertiesRecette_2(){
			return recette_2Repository.findAll();
		}
	//get all properties of Recette 1 
		@GetMapping("/Recette_3")
		public List<Recette_3> getAllPropertiesRecette_3(){
			return recette_3Repository.findAll();
		}
}
