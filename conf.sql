-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 16 août 2021 à 22:46
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `conf`
--

-- --------------------------------------------------------

--
-- Structure de la table `commun`
--

DROP TABLE IF EXISTS `commun`;
CREATE TABLE IF NOT EXISTS `commun` (
  `id` bigint(20) NOT NULL,
  `activation_changement_mdp` bit(1) DEFAULT NULL,
  `delai` int(11) DEFAULT NULL,
  `mode_paiement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taux_tva` int(11) DEFAULT NULL,
  `version_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `commun`
--

INSERT INTO `commun` (`id`, `activation_changement_mdp`, `delai`, `mode_paiement`, `taux_tva`, `version_id`) VALUES
(1, b'0', 10, 'visa', 30, '1');

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Structure de la table `pre_production`
--

DROP TABLE IF EXISTS `pre_production`;
CREATE TABLE IF NOT EXISTS `pre_production` (
  `id` bigint(20) NOT NULL,
  `activation_changement_mdp` bit(1) DEFAULT NULL,
  `delai` int(11) DEFAULT NULL,
  `mode_paiement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taux_tva` int(11) DEFAULT NULL,
  `version_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `pre_production`
--

INSERT INTO `pre_production` (`id`, `activation_changement_mdp`, `delai`, `mode_paiement`, `taux_tva`, `version_id`) VALUES
(1, b'1', 10, 'visa', 17, '1'),
(2, b'0', 40, 'cash', 20, '2');

-- --------------------------------------------------------

--
-- Structure de la table `production`
--

DROP TABLE IF EXISTS `production`;
CREATE TABLE IF NOT EXISTS `production` (
  `id` bigint(20) NOT NULL,
  `activation_changement_mdp` bit(1) DEFAULT NULL,
  `delai` int(11) DEFAULT NULL,
  `mode_paiement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taux_tva` int(11) DEFAULT NULL,
  `version_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `production`
--

INSERT INTO `production` (`id`, `activation_changement_mdp`, `delai`, `mode_paiement`, `taux_tva`, `version_id`) VALUES
(1, b'0', 33, 'cash', 20, '1'),
(2, b'0', 40, 'cash', 30, '2');

-- --------------------------------------------------------

--
-- Structure de la table `recette_1`
--

DROP TABLE IF EXISTS `recette_1`;
CREATE TABLE IF NOT EXISTS `recette_1` (
  `id` bigint(20) NOT NULL,
  `activation_changement_mdp` bit(1) DEFAULT NULL,
  `delai` int(11) DEFAULT NULL,
  `mode_paiement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taux_tva` int(11) DEFAULT NULL,
  `version_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `recette_1`
--

INSERT INTO `recette_1` (`id`, `activation_changement_mdp`, `delai`, `mode_paiement`, `taux_tva`, `version_id`) VALUES
(1, b'0', 20, 'cash', 20, '1'),
(2, b'0', 10, 'visa', 30, '2');

-- --------------------------------------------------------

--
-- Structure de la table `recette_2`
--

DROP TABLE IF EXISTS `recette_2`;
CREATE TABLE IF NOT EXISTS `recette_2` (
  `id` bigint(20) NOT NULL,
  `activation_changement_mdp` bit(1) DEFAULT NULL,
  `delai` int(11) DEFAULT NULL,
  `mode_paiement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taux_tva` int(11) DEFAULT NULL,
  `version_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `recette_2`
--

INSERT INTO `recette_2` (`id`, `activation_changement_mdp`, `delai`, `mode_paiement`, `taux_tva`, `version_id`) VALUES
(1, b'0', 15, 'chèque', 25, '1'),
(2, b'1', 3, 'visa', 20, '2');

-- --------------------------------------------------------

--
-- Structure de la table `recette_3`
--

DROP TABLE IF EXISTS `recette_3`;
CREATE TABLE IF NOT EXISTS `recette_3` (
  `id` bigint(20) NOT NULL,
  `activation_changement_mdp` bit(1) DEFAULT NULL,
  `delai` int(11) DEFAULT NULL,
  `mode_paiement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taux_tva` int(11) DEFAULT NULL,
  `version_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `recette_3`
--

INSERT INTO `recette_3` (`id`, `activation_changement_mdp`, `delai`, `mode_paiement`, `taux_tva`, `version_id`) VALUES
(1, b'0', 45, 'cash', 25, '1'),
(2, b'1', 40, 'chèque', 25, '2');

-- --------------------------------------------------------

--
-- Structure de la table `test`
--

DROP TABLE IF EXISTS `test`;
CREATE TABLE IF NOT EXISTS `test` (
  `id` bigint(20) NOT NULL,
  `activation_changement_mdp` bit(1) DEFAULT NULL,
  `delai` int(11) DEFAULT NULL,
  `mode_paiement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taux_tva` int(11) DEFAULT NULL,
  `version_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `test`
--

INSERT INTO `test` (`id`, `activation_changement_mdp`, `delai`, `mode_paiement`, `taux_tva`, `version_id`) VALUES
(1, b'0', 46, 'visa', 20, '1'),
(2, b'0', 42, 'cash', 30, '2');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
